#include "../FwdAFPCountMonitoringAlg.h"
#include "../FwdAFPJetEffMonitoringAlg.h"
#include "../FwdAFPJetMonitoringAlg.h"
#include "../FwdZDCMonitoringAlg.h"
#include "../HLTHeavyIonMonAlg.h"
#include "../TRTMonitoringAlg.h"
#include "../HLTMBTSMonitoringAlgMT.h"
#include "../HLTMinBiasEffMonitoringAlg.h"
#include "../HLTMinBiasTrkMonAlg.h"
#include "../TrigAFPSidHypoMonitoringAlg.h"

DECLARE_COMPONENT(FwdAFPCountMonitoringAlg)
DECLARE_COMPONENT(FwdAFPJetEffMonitoringAlg)
DECLARE_COMPONENT(FwdAFPJetMonitoringAlg)
DECLARE_COMPONENT(FwdZDCMonitoringAlg)
DECLARE_COMPONENT(HLTHeavyIonMonAlg)
DECLARE_COMPONENT(TRTMonitoringAlg)
DECLARE_COMPONENT(HLTMBTSMonitoringAlgMT)
DECLARE_COMPONENT(HLTMinBiasEffMonitoringAlg)
DECLARE_COMPONENT(HLTMinBiasTrkMonAlg)
DECLARE_COMPONENT(TrigAFPSidHypoMonitoringAlg)
